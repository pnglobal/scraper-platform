<?php

namespace App\Console\Commands;

use App\Jobs\ScrapeStatesJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Scrapers\HoursCenter\Scraper;

class ScrapeStates extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'hourscenter:states';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Scrape all US State information from hourscenter.com';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->output->note("Scraping US state data from hourscenter.com");

		dispatch(new ScrapeStatesJob());
	}
}
