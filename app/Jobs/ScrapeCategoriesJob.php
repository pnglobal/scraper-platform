<?php

namespace App\Jobs;


use App\Scrapers\HoursCenter\Scraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ScrapeCategoriesJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


	protected $scraper;
	protected $city;
	protected $state;

	/**
	 * Create a new job instance.
	 */
	public function __construct($city, $state)
	{
		$this->scraper = new Scraper();
		$this->city    = $city;
		$this->state   = $state;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		try {
			$categories = $this->scraper->scrapeCategories($this->city);

			foreach ($categories as $category) {
				dispatch(new ScrapeCategoryBrandsJob($category, $this->city, $this->state));
			}
		}
		catch(\Exception $ex) {
			Log::error('An error occurred while attempting to scrape category', $ex);
		}


	}
}
