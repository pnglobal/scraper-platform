<?php

namespace App\Jobs;

use App\Models\HoursCenter\Brand;
use App\Models\HoursCenter\Category;
use App\Models\HoursCenter\City;
use App\Models\HoursCenter\State;
use App\Scrapers\HoursCenter\Scraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ScrapeCategoryBrandsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $scraper;
	protected $category;
	protected $city;
	protected $state;

	/**
	 * Create a new job instance.
	 */
	public function __construct($category, $city, $state)
	{
		$this->scraper  = new Scraper();
		$this->category = $category;
		$this->city     = $city;
		$this->state    = $state;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$brands = $this->scraper->scrapeCategoryBrands($this->category);

		foreach ($brands as $brand) {
			dispatch(new ScrapeLocationsJob($brand, $this->category, $this->city, $this->state));
		}
	}
}
