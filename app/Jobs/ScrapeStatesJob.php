<?php

namespace App\Jobs;

use App\Models\HoursCenter\State;
use App\Scrapers\HoursCenter\Scraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ScrapeStatesJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $scraper;

	/**
	 * Create a new job instance.
	 */
	public function __construct()
	{
		$this->scraper = new Scraper();
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		Log::info("Starting state scraper");

		try {
			$states = $this->scraper->scrapeStates();

			foreach ($states as $state) {
				dispatch(new ScrapeCitiesJob($state));
			};
		}
		catch (\Exception $ex) {
			Log::error('An error was encountered while attempting to scrape states', $ex);
		}


	}
}
