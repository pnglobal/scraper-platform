<?php

namespace App\Jobs;


use App\Scrapers\HoursCenter\Scraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\HoursCenter\Location;

class ScrapeLocationsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


	protected $scraper;
	protected $brand;
	protected $category;
	protected $city;
	protected $state;

	/**
	 * Create a new job instance.
	 */
	public function __construct($brand, $category, $city, $state)
	{
		$this->scraper  = new Scraper();
		$this->brand    = $brand;
		$this->category = $category;
		$this->city     = $city;
		$this->state    = $state;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$locations = $this->scraper->scrapeLocations($this->brand);

		foreach ($locations as $brandLocation) {
			$locationData = $this->scraper->scrapeLocationData($brandLocation);
			$location     = new Location();

			if (empty($locationData['id'])) {
				$location = Location::where('id', $locationData['id'])->get()->first();
			}

			$location->hours_center_id = $locationData["id"];
			$location->name            = $this->brand['name'];
			$location->address         = $locationData["address"];
			$location->phone           = $locationData["phone"];
			$location->hours           = $locationData["hours"];

			$location->save();
		}
	}
}
