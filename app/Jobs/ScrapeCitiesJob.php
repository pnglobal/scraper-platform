<?php

namespace App\Jobs;

use App\Models\HoursCenter\City;
use App\Models\HoursCenter\State;
use App\Scrapers\HoursCenter\Scraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ScrapeCitiesJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var Scraper
	 */
	protected $scraper;

	/**
	 * @var State
	 */
	protected $state;

	/**
	 * Create a new job instance.
	 * @param State $state
	 */
	public function __construct($state)
	{
		$this->scraper = new Scraper();
		$this->state   = $state;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$cities = $this->scraper->scrapeCities($this->state);
		foreach ($cities as $city) {
			dispatch(new ScrapeCategoriesJob($city, $this->state));
		}

	}

	/**
	 * The job failed to process.
	 *
	 * @param Exception|\Exception $exception
	 */
	public function failed(\Exception $exception)
	{
		// Send user notification of failure, etc...
		Log::error("Error in ScrapeCities: " . $exception->getMessage());
	}
}
