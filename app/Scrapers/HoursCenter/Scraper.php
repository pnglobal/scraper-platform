<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/14/17
 * Time: 3:42 PM
 */

namespace App\Scrapers\HoursCenter;

use Goutte\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

class Scraper
{

	protected $client;
	protected $config;


	public function __construct()
	{
		$this->config = config("hourscenter");
		$this->client = new Client();
	}

	public function scrapeStates()
	{
		$states = [];

		try {

			$crawler = $this->client->request('GET', $this->config["startUrl"]);
			// Get the latest post in this category and display the titles
			$node = $crawler->filter('div.states ul.state_list > li > a.name');

			if ($node->count() > 0) {
				$states = $node->each(function (Crawler $node) {
					return [
						"href" => $node->attr('href'),
						"name" => $node->text()
					];
				});
			}
		}
		catch (\Exception $ex) {
			throw new \DomainException('Could not load states');
		}

		return Collection::make($states);
	}

	public function scrapeCities($state)
	{
		$cities = [];

		try {
			$crawler = $this->client->request('GET', $state["href"]);

			$node = $crawler->filter('ul.state_list > li > a.name');

			if ($node->count() > 0) {
				$cities = $node->each(function (Crawler $node) {
					return [
						"href" => $node->attr('href'),
						"name" => $node->text()
					];
				});
			}
		}
		catch (\Exception $ex) {
			// do nothing
		}
		return Collection::make($cities);
	}

	public function scrapeCategories($city)
	{
		$categories = [];

		try {
			$crawler = $this->client->request('GET', $city["href"]);
			$node    = $crawler->filter('ul.state_list > li > a.name');
			if ($node->count() > 0) {
				$categories = $node->each(function (Crawler $node) {
					return [
						"href" => $node->attr('href'),
						"name" => $node->text()
					];
				});
			}
		}
		catch (\Exception $ex) {
			// do nothing
		}


		return Collection::make($categories);
	}

	public function scrapeCategoryBrands($category)
	{
		$brands = [];

		try {
			$crawler = $this->client->request('GET', $category["href"]);
			$node    = $crawler->filter('div.cities ul.state_list > li > a.name');

			if ($node->count() > 0) {
				$brands = $node->each(function (Crawler $node) {
					return [
						"href" => $node->attr('href'),
						"name" => $node->text()
					];
				});
			}
		}
		catch (\Exception $ex) {
			// do nothing
		}

		return Collection::make($brands);
	}

	public function scrapeLocations($brand)
	{
		$locations = [];

		try {
			$crawler = $this->client->request('GET', $brand["href"]);
			$node    = $crawler->filter('div.store_list ul.listing_list > li > a');
			if ($node->count() > 0) {
				$locations = $node->each(function (Crawler $node) {
					return [
						"href" => $node->attr('href'),
						"name" => $node->text()
					];
				});
			}
		}
		catch (\Exception $ex) {
			// do nothing
		}

		return Collection::make($locations);
	}

	public function scrapeAddress(Crawler $crawler)
	{
		try {
			$node = $crawler->filter("ul.brand_introduce");

			if ($node->count() == 0) {
				return [];
			}

			$streetAddress = $node->filter("span[itemprop=\"streetAddress\"]")->text();
			$city          = $node->filter("span[itemprop=\"addressLocality\"]")->text();
			$state         = $node->filter("span[itemprop=\"addressRegion\"]")->text();
			$postal_code   = $node->filter("span[itemprop=\"postalCode\"]")->text();

			return [
				"streetAddress" => $streetAddress,
				"city" => $city,
				"state" => $state,
				"postal_code" => $postal_code
			];
		}
		catch (\Exception $ex) {
			return [];
		}
	}

	public function scrapeHours(Crawler $crawler)
	{
		$node  = $crawler->filter("div.hours table.hours_list >  tbody > tr");
		$hours = [];

		if ($node->count() > 0) {
			$hours = $node->each(function (Crawler $hoursNode) {

				try {
					$dayOfWeek = $hoursNode->filter("td.date")->text();
					$hours     = $hoursNode->filter("td.time")->text();

					return [
						"dayOfWeek" => trim(str_replace(":", "", $dayOfWeek)),
						"hours" => $hours
					];
				}
				catch (\Exception $ex) {
//					logger()->error($ex);
					return [];
				}
			});
		}

		return $hours;
	}

	public function scrapePhone(Crawler $crawler)
	{

		$node = $crawler->filter("#iphn > a");
		if ($node->count() > 0) {
			return $node->text();
		}
		return null;
	}

	public function extractLocationId($href)
	{
		$regex = '/\/([0-9].+?)\//';

		preg_match_all($regex, $href, $matches);

		if (is_array($matches) && is_array($matches[0])) {
			return intval($matches[1][0]);
		}

		return null;
	}

	public function scrapeLocationData($location)
	{

		$id = $this->extractLocationId($location["href"]);

		$crawler = $this->client->request('GET', $location["href"]);

		$phone   = $this->scrapePhone($crawler);
		$address = $this->scrapeAddress($crawler);
		$hours   = $this->scrapeHours($crawler);

		$locationData = [
			"id" => $id,
			"address" => $address,
			"phone" => $phone,
			"hours" => $hours
		];

		return $locationData;
	}
}