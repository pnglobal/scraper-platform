<?php
namespace App\Models\HoursCenter;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class City extends Moloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'cities';
}
