<?php
namespace App\Models\HoursCenter;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Brand extends Moloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'brands';
}
