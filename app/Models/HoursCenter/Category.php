<?php

namespace App\Models\HoursCenter;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Category extends Moloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'categories';
}
