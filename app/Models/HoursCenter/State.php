<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/21/17
 * Time: 7:14 PM
 */

namespace App\Models\HoursCenter;


use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class State extends Moloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'states';

	protected $fillable = ["name", "href"];
}
